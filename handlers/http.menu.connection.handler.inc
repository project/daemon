<?php

/**
 * HTTP Menu Server Connection Handler.
 *
 * Implements a class that handles an HTTP Menu server connection.
 * 
 * Coded by: Aymerick Jehanne <aymerick@newlc.com>
 * Sponsored by NewLC: http://www.newlc.com
 */

include_once './'. drupal_get_path('module', 'daemon') .'/nanoserv/handlers/NS_HTTP_Service_Handler.php';

/**
 * HTTP Menu handler
 */
class Daemon_HTTP_Menu_Connection_Handler extends NS_HTTP_Service_Handler {
  // Server connection accepted
  public function on_Accept() {
    drush_print(t('Client accepted'));
  }
  
  // Server disconnected
  public function on_Disconnect() {
    drush_print(t('Disconnected'));
  }

  // HTTP Request received
  public function on_Request($url) {
    $status = 200;

    drush_print(t('Request: '). $url);
    
    daemon_reinit_request($url);
    
    // Fire menu handler
    $return = menu_execute_active_handler();
    
    // Menu status constants are integers; page content is a string.
    if (is_int($return)) {
      switch ($return) {
        case MENU_NOT_FOUND:
          $status = 404;
          $return = t('Not found');
          break;
        case MENU_ACCESS_DENIED:
          $status = 401;
          $return = t('Access denied');
          break;
        case MENU_SITE_OFFLINE:
          $status = 403;
          $return = t('Site offline');
          break;
      }
    }
    
    $this->Set_Response_Status($status);
    
    return $return;
  }
}
