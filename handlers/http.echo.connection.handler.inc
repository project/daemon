<?php

/**
 * HTTP Echo Server Connection Handler.
 *
 * Implements a class that handles an HTTP Echo server connection.
 * 
 * Coded by: Aymerick Jehanne <aymerick@newlc.com>
 * Sponsored by NewLC: http://www.newlc.com
 */

include_once './'. drupal_get_path('module', 'daemon') .'/nanoserv/handlers/NS_HTTP_Service_Handler.php';

/**
 * HTTP Echo handler
 */
class Daemon_HTTP_Echo_Connection_Handler extends NS_HTTP_Service_Handler {
	public function on_Request($url) {
		return t("You asked for url: %url\n", array('%url' => $url));
	}
}
