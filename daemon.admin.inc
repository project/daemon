<?php

/**
 * Daemon module admin pages.
 * 
 * Coded by: Aymerick Jehanne <aymerick@newlc.com>
 * Sponsored by NewLC: http://www.newlc.com
 */


/******************************************************************************
 * Menu callbacks
 ******************************************************************************/

define('DAEMON_ADMIN_LIST_DAEMONS_NB', 25);

/**
 * This page lists all daemons and provides links to edit them.
 */
function daemon_admin_page() {
  $output = '';

  drupal_set_title(t('Administer daemons'));


  // database daemons
  $result = pager_query("SELECT did FROM {daemon_daemon} ORDER BY name", DAEMON_ADMIN_LIST_DAEMONS_NB);

  while ($daemon = db_fetch_object($result)) {
    // get full daemon infos
    $daemon = _daemon_load_daemon($daemon->did);
    
    // listeners
    $listeners = array();
    foreach ($daemon->listeners as $listener) {
      $listeners[] = $listener['name'] . ' ('. $listener['type'] .':'. $listener['port'] .')';
    }
    
    // timers
    $timers = array();
    foreach ($daemon->timers as $timer) {
      // @todo Add a link to edit timer
      $timers[] = $timer['name'];
    }

    $items[] = array(
      $daemon->name,
      $daemon->description,
      implode(',<br />', $listeners),
      implode(',<br />', $timers),
      theme('links', array(
        array('title' => t('edit'), 'href' => "admin/build/daemon/$daemon->name/edit"), 
        array('title' => t('delete'), 'href' => "admin/build/daemon/$daemon->name/delete"),
      ))
    );
  }

  if (!empty($items)) {
    $header = array(t('Name'), t('Description'), t('Listeners'), t('Timers'), t('Actions'));
    $output .= theme('table', $header, $items, array(), t('Existing Daemons'));
    $output .= theme('pager', NULL, DAEMON_ADMIN_LIST_DAEMONS_NB);
  }
  else {
    $output .= t('<p>No custom daemons have currently been defined.</p>');
  }


  // default daemons
  $items = array();
  $default_daemons = _daemon_get_default_daemons();
  
  foreach ($default_daemons as $daemon) {
    $listeners = array();
    foreach ($daemon->listeners as $listener) {
      $listeners[] = $listener['name'] . ' ('. $listener['type'] .':'. $listener['port'] .')';
    }
    
    $items[] = array(
      $daemon->name,
      $daemon->description,
      implode(',<br />', $listeners)
    );
  }
  
  if ($items) {
    $output .= '<br />';
    
    $header = array(t('Name'), t('Description'), t('Listeners'));
    $output .= theme('table', $header, $items, array(), t('Default Daemons'));
  }

  return $output;
}

/**
 * Provide a form to add a daemon.
 */
function daemon_admin_add_page() {
  if ($_POST['op'] == t('Cancel')) {
    drupal_goto('admin/build/daemon');
  }

  // get empty daemon
  $daemon = _daemon_get_empty_daemon();
  
  drupal_set_title(t('Add a Daemon'));
  return drupal_get_form('daemon_edit_daemon', $daemon);
}

/**
 * List all defined timers.
 */
function daemon_admin_all_timers_page() {
  $timers = _daemon_timers();

  $header = array(t('Timer name'), t('Timer description'), t('Timeout'), t('Used by'), t('Actions'));
  
  $rows = array();
  foreach ($timers as $timer) {
    $row = array();
    
    $row[] = $timer['name'];
    $row[] = $timer['description'];
    $row[] = $timer['timeout'] .' '. t('second(s)');
    
    $daemons = array();
    $result = db_query("SELECT dd.did as did, dd.name as name FROM {daemon_daemon} dd LEFT JOIN {daemon_timer_daemon} dtd ON dtd.did = dd.did AND dtd.tid = %d ORDER BY dd.name ASC", $timer['tid']);
    while ($daemon = db_fetch_object($result)) {
      $daemons[] = l($daemon->name, 'admin/build/daemon/'. $daemon->name);
    }
    $row[] = implode(', ', $daemons);
    $row[] = theme('links', array(array('title' => t('delete'), 'href' => "admin/build/daemon/del_timer/". $timer['tid'])));

    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);

  return $output;
}

/**
 * Provide a form to confirm deletion of a timer.
 */
function daemon_admin_timer_delete_confirm($timer) {
  $form['tid'] = array('#type' => 'value', '#value' => $timer['tid']);

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $timer['name'])),
    $_GET['destination'] ? $_GET['destination'] : 'admin/build/daemon/timers',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
  
  return $form;
}

/**
 * Handle the submit button to delete a daemon.
 */
function daemon_admin_timer_delete_confirm_submit($form_id, $form) {
  _daemon_delete_timer($form['tid']);
}

/**
 * Provide a form to edit a daemon.
 */
function daemon_admin_edit_page($daemon) {
  if ($_POST['op'] == t('Cancel')) {
    drupal_goto('admin/build/daemon');
  }

  if ($_POST['op'] == t('Delete')) {
    drupal_goto("admin/build/daemon/". $daemon->name ."/delete");
  }

  drupal_set_title(t('Edit daemon %name', array('%name' => $daemon->name)));
  return drupal_get_form('daemon_edit_daemon', $daemon);
}

/**
 * Provide a form to manage a daemon timers.
 */
function daemon_admin_timers_page($daemon) {
  $timers = _daemon_timers($daemon->did);
  
  $header = array(t('Timer name'), t('Timer description'), t('Timeout'), t('Actions'));
  
  $rows = array();
  foreach ($timers as $timer) {
    $row = array();
    
    $row[] = $timer['name'];
    $row[] = $timer['description'];
    $row[] = $timer['timeout'] .' '. t('second(s)');
    
    $row[] = theme('links', array(
               array('title' => t('edit'), 'href' => "admin/build/daemon/". $daemon->name ."/timers/". $timer['tid']), 
               array('title' => t('remove'), 'href' => "admin/build/daemon/". $daemon->name ."/timers/". $timer['tid'] ."/remove"),
             ));
    
    $rows[] = $row;
  }
  
  $output = theme('table', $header, $rows);
  
  return $output;
}

/**
 * Provide a form to add a timer to a deamon.
 */
function daemon_admin_add_timer_page($daemon) {
  $output = drupal_get_form('daemon_add_existing_timer', $daemon);
  $output .= drupal_get_form('daemon_add_new_timer', $daemon);
  return $output;
}

/**
 * Provide a form to confirm deletion of a daemon.
 */
function daemon_admin_delete_confirm($daemon) {
  $form['did'] = array('#type' => 'value', '#value' => $daemon->did);
  
  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $daemon->name)),
    $_GET['destination'] ? $_GET['destination'] : 'admin/build/daemon',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
  
  return $form;
}

/**
 * Handle the submit button to delete a daemon.
 */
function daemon_admin_delete_confirm_submit($form_id, $form) {
  _daemon_delete_demon($form['did']);
}

/**
 * Provide a form to edit a timer
 */
function daemon_admin_edit_timer_page($daemon, $timer) {
  $form = array();
  
  // @todo daemon_admin_edit_timer_page()
  
  return $form;
}

/**
 * Provide a form to confirm removing of a timer from a daemon.
 */
function daemon_admin_timer_remove_confirm($daemon, $timer) {
  $form['did'] = array('#type' => 'value', '#value' => $daemon->did);
  $form['tid'] = array('#type' => 'value', '#value' => $timer['tid']);

  $form = confirm_form($form,
    t('Are you sure you want to remove %timer from %daemon ?', array('%timer' => $timer['name'], '%daemon' => $daemon->name)),
    $_GET['destination'] ? $_GET['destination'] : 'admin/build/daemon/'. $daemon->name .'/timers',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
  
  return $form;
}

/**
 * Handle the submit button to delete a timer.
 */
function daemon_admin_timer_remove_confirm_submit($form_id, $form) {
  _daemon_disassociate_timer($form['did'], $form['tid']);
}


/******************************************************************************
 * Daemon form
 ******************************************************************************/

/**
 * Build main daemon edit form.
 */
function daemon_edit_daemon($daemon) {
  $form = array();

  $form['did'] = array(
    '#type' => 'value',
    '#value' => $daemon->did,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $daemon->name,
    '#size' => 20,
    '#maxlength' => 32,
    '#description' => t('The unique identifier of the daemon. Only alphanumeric and _ allowed here'),
    '#required' => true,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $daemon->description,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('A description of the daemon for the admin list.'),
  );

  // listeners
  $form['listeners'] = array('#tree' => TRUE);
  
  $delta = 0;
  foreach ($daemon->listeners as $listener) {
    $form['listeners'][$delta]['id'] = array(
      '#type' => 'select',
      '#title' => t('Listener @number', array('@number' => $delta)),
      '#default_value' => $listener['id'],
      '#options' => array('' => t('--- Select ---')) + _daemon_listeners('names'),
    );
    
    $form['listeners'][$delta]['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => $listener['type'],
      '#options' => _daemon_listener_types(),
    );
    
    $form['listeners'][$delta]['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#default_value' => $listener['port'],
      '#size' => 5,
      '#maxlength' => 5,
    );

    $delta++;
  }
  
  foreach (range($delta, $delta + 2) as $delta) {
    $form['listeners'][$delta]['id'] = array(
      '#type' => 'select',
      '#title' => t('Listener @number', array('@number' => $delta)),
      '#options' => array('' => t('--- Select ---')) + _daemon_listeners('names'),
    );
    
    $form['listeners'][$delta]['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => _daemon_listener_types(),
    );
    
    $form['listeners'][$delta]['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#size' => 5,
      '#maxlength' => 5,
    );
  }
  
  // actions
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  $form['actions']['save_and_edit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and edit'),
  );
  
  if ($daemon->did) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  
  return $form;
}

/**
 * Theme main daemon edit form.
 */
function theme_daemon_edit_daemon($form) {
  // extract listeners
  $rows = array();
  foreach (element_children($form['listeners']) as $index) {
    unset($form['listeners'][$index]['id']['#title']);
    unset($form['listeners'][$index]['type']['#title']);
    unset($form['listeners'][$index]['port']['#title']);

    $rows[] = array(
      drupal_render($form['listeners'][$index]['id']),
      drupal_render($form['listeners'][$index]['type']),
      drupal_render($form['listeners'][$index]['port']),
    );
  }
  unset($form['listeners']);
  
  // extract actions
  $actions = $form['actions'];
  unset($form['actions']);

  // theme form
  $output = drupal_render($form);

  // theme listeners table
  $header = array(t('Listener Name'), t('Type'), t('Port'));
  $output .= theme('table', $header, $rows, array(), t('Listeners'));
  
  // theme actions
  $actions['#prefix'] = '<br />';
  $output .= drupal_render($actions);
  
  return $output;
}

/**
 * Validate daemon form.
 */
function daemon_edit_daemon_validate($form_id, $daemon, $form) {
  $op = $daemon['op'];

  if (($op != t('Save')) && ($op != t('Save and edit'))) {
    // only validate on saving
    return;
  }
  
  if (empty($daemon['name'])) {
    form_error($form['name'], t('Daemon name is required.'));
  }

  // daemon name must be alphanumeric or underscores, no other punctuation.
  if (preg_match('/[^a-zA-Z0-9_]/', $daemon['name'])) {
    form_error($form['name'], t('Daemon name must be alphanumeric or underscores only.'));
  }
  
  // test uniqueness of name
  $did = db_result(db_query("SELECT did FROM {daemon_daemon} WHERE name='%s'", $daemon['name']));
  if ($did && ($did != $daemon['did'])) {
    form_error($form['name'], t('Daemon name already in use.'));
  }
  
  // check that a port is selected for each listener
  foreach ($daemon['listeners'] as $index => $listener) {
    if (!empty($listener['id'])) {
      if (empty($listener['port'])) {
        form_error($form['listeners'][$index]['port'], t('Daemon port is required.'));
      }
      else if (!is_numeric($listener['port'])) {
        form_error($form['listeners'][$index]['port'], t('Invalid port number.'));
      }
    }
  }
}

/**
 * Submit daemon form.
 */
function daemon_edit_daemon_submit($form_id, $form) {
  if ($form['op'] == t('Cancel')) {
    drupal_goto('admin/build/daemon');
  }
  
  // get daemon
  $daemon = (object) $form;
  _daemon_fill_listeners($daemon);

  // save daemon
  $did = _daemon_save_daemon($daemon);
  
  if ($form['did']) {
    drupal_set_message(t('Daemon successfully saved.'));
  }
  else {
    drupal_set_message(t('Daemon successfully added.'));
  }

  if ($form['op'] == t('Save')) {
    $args = explode('/', $_GET['q']);
    if ($args[0] == 'admin' && $args[1] == 'build' && $args [2] == 'daemon') {
      return 'admin/build/daemon';
    }
    array_pop($args);
    return implode('/', $args);
  }
  
  if ($form['op'] == t('Save and edit')) {
    return "admin/build/daemon/". $daemon->name ."/edit";
  }
}


/******************************************************************************
 * Timer forms
 ******************************************************************************/

/**
 * Provide a form to add an existing timer to a daemon.
 */
function daemon_add_existing_timer($daemon) {
  $form = array();

  // get all possible timers
  $timers = _daemon_timers();

  // remove timers already associated with that daemon
  foreach ($daemon->timers as $daemon_timer) {
    foreach ($timers as $index => $timer) {
      if ($daemon_timer['name'] == $timer['name']) {
        unset($timers[$index]);
      }
    }
  }
  
  // build options
  $timers_options = array();
  foreach ($timers as $timer) {
    $timers_options[$timer['tid']] = $timer['name'] .' '. t('(!nb seconds)', array('!nb' => $timer['timeout']));
  }

  if (!empty($timers_options)) {
    $form['existing'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add existing timer'),
    );
    
    $form['existing']['timer'] = array(
      '#type' => 'select',
      '#options' => $timers_options,
      '#required' => true,
    );
    
    $form['existing']['did'] = array(
      '#type' => 'value',
      '#value' => $daemon->did,
    );
  
    $form['existing']['daemon_name'] = array(
      '#type' => 'value',
      '#value' => $daemon->name,
    );
    
    $form['existing']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add timer'),
    );
  }
  
  return $form;
}

/**
 * Submit 'add existing timer' form.
 */
function daemon_add_existing_timer_submit($form_id, $form_values) {
  // save timer
  if ($tid = _daemon_associate_timer($form_values['did'], $form_values['timer'])) {
    drupal_set_message(t('Timer added to daemon %daemon_name.', array('%daemon_name' => $form_values['daemon_name'])));
    return 'admin/build/daemon/'. $form_values['did'] .'/timers';
  }
  else {
    drupal_set_message(t('Failed to add timer.'), 'error');
  }
  
  return 'admin/build/daemon/'. $form_values['daemon_name'];
}

/**
 * Provide a form to add a new timer to a daemon.
 */
function daemon_add_new_timer($daemon) {
  $form = array();
  
  $form['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create new timer'),
  );
  
  $form['new']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => '',
    '#size' => 20,
    '#maxlength' => 32,
    '#description' => t('Timer name. Only alphanumeric and _ allowed here'),
    '#required' => true,
  );
  
  $form['new']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('A description of the timer for the admin list.'),
  );
  
  $form['new']['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout'),
    '#default_value' => '60',
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Number of seconds between each timeout.'),
    '#required' => true,
  );
  
  $form['new']['php'] = array(
    '#type' => 'textarea',
    '#title' => t('PHP Code'),
    '#default_value' => '',
    '#description' => t('This PHP code will be executed at each timeout. Enter code between %php. Note that executing incorrect PHP code can break your site.', array('%php' => '<?php ?>')),
  );

  $form['new']['did'] = array(
    '#type' => 'value',
    '#value' => $daemon->did,
  );

  $form['new']['daemon_name'] = array(
    '#type' => 'value',
    '#value' => $daemon->name,
  );
  
  $form['new']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create timer'),
  );
  
  return $form;
}

/**
 * Validate 'add new timer' form.
 */
function daemon_add_new_timer_validate($form_id, $form_values) {
  // name must be alphanumeric or underscores, no other punctuation.
  if (preg_match('/[^a-zA-Z0-9_]/', $form_values['name'])) {
    form_set_error('name', t('Timer name must be alphanumeric or underscores only.'));
  }
  
  // test uniqueness of name
  if (db_result(db_query("SELECT tid FROM {daemon_timer} WHERE name='%s'", $form_values['name']))) {
    form_set_error('name', t('Timer name already in use.'));
  }

  // timeout value
  if (!is_numeric($form_values['timeout'])) {
    form_set_error('timeout', t('Invalid timeout value.'));
  }
}

/**
 * Submit 'add new timer' form.
 */
function daemon_add_new_timer_submit($form_id, $form_values) {
  // save timer
  if ($tid = _daemon_save_timer((object) $form_values)) {
    // associate timer to daemon
    if (_daemon_associate_timer($form_values['did'], $tid)) {
      drupal_set_message(t('Created timer %name, and added to daemon %daemon_name.', array('%name' => $form_values['name'],
                                                                                           '%daemon_name' => $form_values['daemon_name'])));
      return 'admin/build/daemon/'. $form_values['did'] .'/timers';
    }
    else {
      drupal_set_message(t('Created timer %name, but failed to associate with daemon %daemon_name.',
                           array('%name' => $form_values['name'], '%daemon_name' => $form_values['daemon_name']), 'error'));
    }
  }
  else {
    drupal_set_message(t('Failed to save timer %name', array('%name' => $form_values['name'])), 'error');
  }
  
  return 'admin/build/daemon/'. $form_values['daemon_name'];
}
