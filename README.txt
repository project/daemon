DAEMON MODULE
--------------------------------------------
Coder: Aymerick Jehanne <aymerick@newlc.com>
Sponsor: NewLC - http://www.newlc.com
--------------------------------------------

Thanks to the daemon module you can run daemon instances of a drupal site.


DEPENDENCY
----------
This module requires:
  - the drush module (http://drupal.org/project/drush)
  - the nanoserv library (http://nanoserv.si.kz)
  - PHP 5.1+.


INSTALLATION
------------
1. Install the drush module (http://drupal.org/project/drush).

2. Extract the daemon module tarball and place the entire daemon directory
  into your Drupal setup (e.g. in sites/all/modules).

3. Download the nanoserv library (version 1.1.0) from
  http://pear.si.kz/index.php?package=nanoserv and place the 'nanoserv' subfolder
  in the daemon module directory (e.g. as /sites/all/modules/deamon/nanoserv).
  
   So the directory structure should look like this:
      + sites/
        + all/
          + modules/
            + daemon/
              - daemon.info
              - daemon.install
              - daemon.module
              - handlers/
              + nanoserv/
                - Changelog
                - examples/
                - handlers/
                - mkhugenanoserv.sh
                - noanoserv.php
                - README
              - README.txt

4. Enable this module as any other Drupal module by navigating to
  administer > site building > modules


CONFIGURATION
-------------
1. The daemon module settings and further configuration instructions
  can be found by navigating to:
  administer > site building > daemons
